# Define the VPC
resource "aws_vpc" "example_vpc" {
  cidr_block = "10.0.0.0/16"
  enable_dns_support = true
  enable_dns_hostnames = true

  tags = {
    Name = "Example-VPC"
  }
}

# Create a public subnet within the VPC
resource "aws_subnet" "public_subnet" {
  vpc_id     = aws_vpc.example_vpc.id
  cidr_block = "10.0.1.0/24"
  availability_zone = "us-east-1a"  

  tags = {
    Name = "Public-Subnet"
  }
}

# Create a security group allowing HTTPS, RDS, and ICMP traffic
resource "aws_security_group" "example_security_group" {
  name        = "example-security-group"
  description = "Example security group"

  vpc_id = aws_vpc.example_vpc.id

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 3306
    to_port     = 3306
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = -1
    to_port     = -1
    protocol    = "icmp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "Example-Security-Group"
  }
}

# Create an IAM role for S3 full access
resource "aws_iam_role" "s3_full_access_role" {
  name = "S3FullAccessRole"

  assume_role_policy = jsonencode({
    Version = "2012-10-17",
    Statement = [{
      Effect    = "Allow",
      Principal = {
        Service = "ec2.amazonaws.com"
      },
      Action    = "sts:AssumeRole"
    }]
  })
}

# Attach an IAM policy granting full S3 access to the role
resource "aws_iam_policy_attachment" "s3_full_access_attachment" {
  name       = "s3-full-access-policy-attachment"
  roles      = [aws_iam_role.s3_full_access_role.name]
  policy_arn = "arn:aws:iam::aws:policy/AmazonS3FullAccess"
}

# Launch EC2 instances in the public subnet with the created security group and IAM role
resource "aws_instance" "example_instances" {
  count         = 2
  ami           = "ami-0230bd60aa48260c6"   
  instance_type = "t2.micro"
  subnet_id     = aws_subnet.public_subnet.id
  security_groups = [aws_security_group.example_security_group.id]
  
#   iam_instance_profile = aws_iam_role.s3_full_access_role.name

  tags = {
    Name = "Example-Instance-${count.index + 1}"
  }
}
